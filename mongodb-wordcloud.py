#!/usr/bin/env python3
import argparse
import logging
import os
import sys
from os import path

import en_core_web_sm
import numpy as np
from PIL import Image
from googletrans import Translator
from wordcloud import WordCloud

from analyzer.modules import MongoRepository

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S',
    handlers=[logging.StreamHandler(sys.stdout)])

nlp = en_core_web_sm.load()
base_dir = path.dirname(__file__) if "__file__" in locals() else os.getcwd()
translator = Translator()
symbols_to_replace = {'&': ' ', '#': ' ', '$': ' ', '£': ' ', '(': ' ', ')': ' ', '%': ' ', ':': ' ', '+': ' ',
                      '-': ' ', '*': ' ', '/': ' ', '<': ' ', '=': ' ', '>': ' ', '?': ' ', '@': ' ', '[': ' ',
                      ']': ' ', "\\": ' ', '^': ' ', '_': ' ', '`': ' ', '{': ' ', '}': ' ', '|': ' ', '~': ' ',
                      '”': ' ', '\t': ' ', '\n': ' ', '\r': ' ', '\v': ' ', '\f': ' '}


def extract_tokens(file, orgs=True):
    content = file.read()
    return extract_tokens_from_str(content, orgs)


def extract_tokens_from_list(text_list, orgs=True):
    content = ' '
    content = content.join(text_list)
    return extract_tokens_from_str(content, orgs)


def extract_tokens_from_str(content, orgs=True):
    # Pre-processing symbols
    symbols_translator = str.maketrans(symbols_to_replace)
    content = content.translate(symbols_translator)
    doc = nlp(content)

    # Retrieve all ORGs and PERSONs
    org_and_persons = []
    # print([(ent.text, ent.label_) for ent in doc.ents])
    for ent in doc.ents:
        if ent.label_ == 'PERSON' or ent.label_ == 'ORG':
            org_and_persons.append(ent.text)

    # Tokenize ORGs and PERSONs for filtering
    all_org_persons = ''
    for org_and_person in org_and_persons:
        all_org_persons += org_and_person + ' '
    org_persons_doc = nlp(all_org_persons)
    org_persons_tokens = []
    for token in org_persons_doc:
        org_persons_tokens.append(token.text)

    # Tokenize all data
    result = []
    for token in doc:
        if not token.is_punct and not token.is_stop:
            if token.text not in org_persons_tokens:
                if (len(token.text) == 1 and token.pos_ == 'NUM') or len(token.text) > 1:
                    result.append(token.lemma_.lower().strip())

    if orgs:
        result = org_and_persons + result

    # Exclude stop words
    stop_words = load_stop_words()
    result = [k.lower().strip() for k in result if k not in stop_words and k is not '']

    return result


def generate_wordcloud(tokens):
    result = ','.join(tokens)
    cloud_mask = np.array(Image.open(path.join(base_dir, "analyzer/resources/masks/mask_cloud.png")))
    word_cloud = WordCloud(background_color="white", mask=cloud_mask, font_path='analyzer/resources/fonts/msyh.ttf').generate(result)
    word_cloud.to_file('word_cloud.png')


def translate(tokens, language_code):
    num_of_tokens = len(tokens)
    result = []
    i = 0
    step = 500
    while i < num_of_tokens:
        end_index = i + step
        if end_index > num_of_tokens:
            end_index = num_of_tokens
        content = '>'.join(tokens[i:end_index])
        translation = translator.translate(content, src='en', dest=language_code)
        logging.debug(f'Translated tokens {translation}')

        translated_tokens = translation.text.split('>')
        result = result + [k.strip() for k in translated_tokens]
        i = end_index
    return correct_translation(language_code, result)


def correct_translation(language_code, tokens):
    translation_map_file = path.join(base_dir, f'analyzer/resources/translation-map-{language_code}.csv')
    translation_map = {}
    if path.exists(translation_map_file):
        with open(translation_map_file) as f:
            translation_map_lines = f.readlines()
        for line in translation_map_lines:
            split_line = line.strip().split(',')
            translation_map[split_line[0]] = split_line[1]
        logging.info(f'Loaded {language_code} transaction map: {translation_map}')
        for i, token in enumerate(tokens):
            if translation_map.get(token) is not None:
                tokens[i] = translation_map.get(token)
    return tokens


def load_data(collection, data_entry):
    docs = MongoRepository.find_document(collection, {})
    details = []
    for doc in docs:
        details.append(doc[data_entry])
    return details


def load_stop_words():
    stop_words_file = path.join(base_dir, "analyzer/resources/stop-words.txt")
    with open(stop_words_file) as f:
        stop_words_content = f.readlines()
    stop_words = [x.strip() for x in stop_words_content]
    logging.info(f'Loaded stop words: {stop_words}')
    return stop_words


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-df", "--datafile", type=str, help="path to data file")
    parser.add_argument("-db", "--database", type=str, help="MongoDB database name")
    parser.add_argument("-c", "--collection", type=str, help="MongoDB collection name")
    parser.add_argument("-de", "--dataentry", type=str, help="MongoDB collection data entry")
    parser.add_argument("-t", "--translate", type=str, help="target language code to translate (ISO639-1)")
    args = parser.parse_args()

    if len(sys.argv) < 2:
        parser.print_usage()
        sys.exit(1)

    if args.datafile:
        # "analyzer/samples/data_sample.txt"
        data_sample_path = path.join(base_dir, args.datafile)
        sample_file = open(data_sample_path, 'r')
        tokens = extract_tokens(sample_file, False)
        logging.info(f'{len(tokens)} tokens are retrieved')
        logging.info(f'Retrieved tokens {tokens}')
        if args.translate:
            translated = translate(tokens, args.translate)
            logging.info(f'{len(translated)} tokens are translated to {args.translate}')
            logging.info(f'Translated tokens {translated}')
            generate_wordcloud(translated)
        else:
            generate_wordcloud(tokens)

    if args.database:
        MongoRepository.db = MongoRepository.client[args.database]
        content = load_data(args.collection, args.dataentry)
        tokens = extract_tokens_from_list(content, False)
        logging.info(f'{len(tokens)} tokens are retrieved')
        logging.debug(f'Retrieved tokens {tokens}')
        if args.translate:
            translated = translate(tokens, args.translate)
            logging.info(f'{len(translated)} tokens are translated to {args.translate}')
            logging.debug(f'Translated tokens {translated}')
            generate_wordcloud(translated)
        else:
            generate_wordcloud(tokens)


main()
