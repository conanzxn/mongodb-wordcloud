import os

from pymongo import MongoClient

MONGO_USERNAME = os.getenv('MONGO_USERNAME')
MONGO_PASSWORD = os.getenv('MONGO_PASSWORD')
MONGO_URL = os.getenv('MONGO_URL')

client = MongoClient('mongodb+srv://{}:{}@{}/admin?retryWrites=true&w=majority'
                     .format(MONGO_USERNAME, MONGO_PASSWORD, MONGO_URL))
db = {}


def insert(collection, data):
    db[collection].insert_one(data)


def read_collection(collection):
    documents = []
    cursor = db[collection].find({})
    for document in cursor:
        documents.append(document)
    return documents


def find_document(collection, query):
    documents = []
    cursor = db[collection].find(query)
    for document in cursor:
        documents.append(document)
    return documents


def delete_document(collection, query):
    return db[collection].delete_many(query).deleted_count
