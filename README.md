# MongoDB Wordcloud

The mongodb-wordcloud retrieves text data from MongoDB and produces a word cloud image.

The default mask is `analyzer/resources/masks/mask_cloud.png`

The default font is `analyzer/resources/fonts/msyh.ttf`

---

![alt text](analyzer/samples/word_cloud_sample.png "Sample Word Cloud Image")

## MongoDB Connection Configuration
Please ensure the following environment varibles are configued. 

```shell
MONGO_USERNAME
MONGO_PASSWORD
MONGO_URL
```

## Command Line Usage

### Setup
```shell
make init
```

### Usage

```shell
usage: mongodb-wordcloud.py [-h] [-df DATAFILE] [-db DATABASE] [-c COLLECTION]
                            [-de DATAENTRY] [-t TRANSLATE]

optional arguments:
  -h, --help            show this help message and exit
  -df DATAFILE, --datafile DATAFILE
                        path to data file
  -db DATABASE, --database DATABASE
                        MongoDB database name
  -c COLLECTION, --collection COLLECTION
                        MongoDB collection name
  -de DATAENTRY, --dataentry DATAENTRY
                        MongoDB collection data entry
  -t TRANSLATE, --translate TRANSLATE
                        target language code to translate (ISO639-1)
```

### Example Commands
#### Create Word Cloud from Data File
```shell
python3 mongodb-wordcloud.py --datafile analyzer/samples/data_sample.txt
```

#### Create Word Cloud from MongoDB
```shell
python3 mongodb-wordcloud.py --collection my_collection --dataentry detail
```
A sample piece of stored data is as follows.
```json
{
  "date": "2020-03-02",
  "author": "Mathew Murray",
  "title": "Big Sing",
  "detail": "In our session with Mrs Clark of Big Sing,our children had lots of fun singing to their favourite songs and some new song,and performing a variety of dance style as the music required so."
}
```

#### Translate into Another Language
```shell
# Translate local file loaded data from English to Chinese
python3 mongodb-wordcloud.py --datafile analyzer/samples/data_sample.txt --translate zh-cn

# Translate MongoDB loaded data from English to Japanese
python3 mongodb-wordcloud.py --collection my_collection --dataentry detail --translate ja
```